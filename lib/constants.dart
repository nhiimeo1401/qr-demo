import 'package:flutter/material.dart';

const kBackground = Color.fromARGB(125, 143, 143, 143);
const kBlack60 = Color.fromARGB(150, 0, 0, 0);
const kOrange = Color.fromARGB(255, 255, 140, 0);
const kOrange80 = Color.fromARGB(150, 255, 140, 0);
